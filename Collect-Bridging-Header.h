//
//  Collect-Bridging-Header.h
//  Collect
//
//  Created by Jose D Leal on 11/21/15.
//  Copyright © 2015 PpDs. All rights reserved.
//

#ifndef Collect_Bridging_Header_h
#define Collect_Bridging_Header_h

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "KontaktSDK.h"


#endif /* Collect_Bridging_Header_h */
