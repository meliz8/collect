//
//  CollectionViewCell.swift
//  collectionView
//
//  Created by Melissa Zellhuber on 10/15/14.
//  Copyright (c) 2014 mzellhuber. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell, KTKLocationManagerDelegate, KTKBluetoothManagerDelegate {
    let locationManager : KTKLocationManager = KTKLocationManager();
    let bluetoothManager : KTKBluetoothManager = KTKBluetoothManager();
    let client : KTKClient = KTKClient();
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    @IBAction func cartButton(sender: AnyObject) {
        
        
    }
    func locationManager(locationManager: KTKLocationManager!, didChangeState state: KTKLocationManagerState, withError error: NSError!) {}
    
    func locationManager(locationManager: KTKLocationManager!, didEnterRegion region: KTKRegion!) {}
    
    func locationManager(locationManager: KTKLocationManager!, didExitRegion region: KTKRegion!) {}
    
    func locationManager(locationManager: KTKLocationManager!, didRangeBeacons beacons: [AnyObject]!) {}
    
    // MARK: - KTKBluetoothManagerDelegate
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeDevices devices: Set<NSObject>!) {
        // println("There are \(devices.count) KTKiBeacons devices")
    }
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeEddystones eddystones: Set<NSObject>!) {
        //println("There are \(eddystones.count) Eddystones in range")
        //self.labelCounterEddystones.text = "\(eddystones.count)"
    }
}
