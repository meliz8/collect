//
//  ViewController.swift
//  Collect
//
//  Created by Jose D Leal on 11/21/15.
//  Copyright © 2015 PpDs. All rights reserved.
//
// Our API KEY is ktTnlnyDbvOuEwBSBxCjxPgJnKuKExkp
//

import UIKit
import Foundation

class ViewController: UIViewController, KTKLocationManagerDelegate, KTKBluetoothManagerDelegate {
    
    let locationManager : KTKLocationManager = KTKLocationManager();
    let bluetoothManager : KTKBluetoothManager = KTKBluetoothManager();
    let colors = [
        58795  : UIColor(red: 215/255,  green: 40/255,   blue: 40/255,   alpha: 1),      // red
        //52056  : UIColor(red: 60/255,    green: 223/255,  blue: 60/255,    alpha: 1),    // green
        //43564  : UIColor(red: 60/255,    green: 223/255,  blue: 60/255,    alpha: 1),    // green
        //48137  : UIColor(red: 68/255,   green: 129/255,  blue: 172/255,  alpha: 1),      // blue
    ]
    let client : KTKClient = KTKClient();
    
    @IBOutlet var labelCounterBeacons: UILabel!
    
    //@IBOutlet weak var labelCounterBeacons: UILabel!
    //@IBOutlet weak var labelCounterEddystones: UILabel!
    // ------------ LED and Door CURL VARIABLES ------------

    var turnLedOn = "https://api.particle.io/v1/devices/55ff6d065075555324151487/prenderLed"
    var bodyData = "arg=led1&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    var bodyData2 = "arg=led2&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    
    var openDoor = "https://api.particle.io/v1/devices/55ff6d065075555324151487/abrirPuerta"
    //var requestOpenDoor = "{ \"arg\": \"A5B7\", \"access_token\": \"54294140ea7a5b2aa3544185e91daf9508eb6d23\" }"

    var requestOpenDoor = "arg=A5B7&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    var LEDFlag = false;
    var DoorFlag = false;

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if (KTKLocationManager.canMonitorBeacons())
        {
            let region : KTKRegion = KTKRegion()
            region.uuid = "f7826da6-4fa2-4e98-8024-bc5b71e0893e" // kontakt.io proximity UUID
            
            self.locationManager.setRegions([region])
            self.locationManager.delegate = self
        }
        
        self.bluetoothManager.delegate = self
        //self.client.apiKey = "ktTnlnyDbvOuEwBSBxCjxPgJnKuKExkp"
        
    }
    
    override func viewDidAppear(animated: Bool) {
        self.locationManager.startMonitoringBeacons()
        self.bluetoothManager.startFindingDevices()
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.locationManager.stopMonitoringBeacons()
        self.bluetoothManager.stopFindingDevices()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - KTKLocationManagerDelegate
    
    func locationManager(locationManager: KTKLocationManager!, didChangeState state: KTKLocationManagerState, withError error: NSError!) {
        if (state == .Failed)
        {
            print("Something went wrong with your Location Services settings. Check your settings.");
        }
    }
    
    func locationManager(locationManager: KTKLocationManager!, didEnterRegion region: KTKRegion!) {
        print("Enter region \(region.identifier)")
    }
    
    func locationManager(locationManager: KTKLocationManager!, didExitRegion region: KTKRegion!) {
        print("Exit region \(region.identifier)")
    }
    
    func locationManager(locationManager: KTKLocationManager!, didRangeBeacons beacons: [AnyObject]!) {
        
        // All the beacons minus all the beacons with proximity 0.
        //let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
        let myBlackBeacon = beacons.filter{ $0.minor == 58795}
        // print(knownBeacons)
        let THEBeacon = myBlackBeacon[0] as! CLBeacon;
        print(THEBeacon)
        self.view.backgroundColor = self.colors[Int(THEBeacon.minor)];
        let distance = THEBeacon.proximity.rawValue
        if (distance == 3){
            // FAR : Turn LED ON
            print("The beacon is FAAAAAR away. Doing... Nothing")
            
//            HTTPPostJSON(turnLedOn, bodyData: bodyData2.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
//                if error != nil {
//                    print(error)
//                } else {
//                    print(data)
//                    
//                }
//            })
        } else if(distance == 2){
            // NEAR : Open Door
            print("The beacon is NEARBY. Doing... LED ON")
            if (!LEDFlag){

                HTTPPostJSON(turnLedOn, bodyData: bodyData.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
                    if error != nil {
                        print(error)
                    } else {
                        print(data)
                        
                    }
                })
                LEDFlag = true;
            }
            
            
        } else if(distance == 1){
            // RIGHT NEXT : Nothing really
            print("The beacon is RIGHT NEXT. Doing... OPEN DOOR")
            if(!DoorFlag){
                HTTPPostJSON(openDoor, bodyData: requestOpenDoor.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
                    if error != nil {
                        print(error)
                    } else {
                        print(data)
                        
                    }
                })
                DoorFlag = true;
            }
            
            
        } else {
            // UNKNOWN : Nothing at all
            print("The beacon is God knows where")
        }

        
        // print("There are \(beacons.count) iBeacons monitored")
    }
    
    // MARK: - KTKBluetoothManagerDelegate
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeDevices devices: Set<NSObject>!) {
        // println("There are \(devices.count) KTKiBeacons devices")
    }
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeEddystones eddystones: Set<NSObject>!) {
        //println("There are \(eddystones.count) Eddystones in range")
        //self.labelCounterEddystones.text = "\(eddystones.count)"
    }
    
    func HTTPPostJSON(url: String,
        bodyData: NSData, //AnyObject,
        callback: (String, String?) -> Void) {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPMethod = "POST"
            request.addValue("application/x-www-form-urlencoded",
                forHTTPHeaderField: "Content-Type")
            request.HTTPBody = bodyData //data
            HTTPsendRequest(request,callback: callback)
    }
    
    func HTTPsendRequest(request: NSMutableURLRequest, callback: (String, String?) -> Void) {
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(
            request, completionHandler :
            {
                data, response, error in
                if error != nil {
                    callback("", (error!.localizedDescription) as String)
                } else {
                    callback(
                        NSString(data: data!, encoding: NSUTF8StringEncoding) as! String,
                        nil
                    )
                }
        })
        
        task.resume()
    }
}