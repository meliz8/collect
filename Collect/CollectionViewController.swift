//
//  CollectionViewController.swift
//  collectionView
//
//  Created by Melissa Zellhuber on 10/15/14.
//  Copyright (c) 2014 mzellhuber. All rights reserved.
//

import UIKit
import Foundation

let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController, KTKLocationManagerDelegate, KTKBluetoothManagerDelegate {
    let locationManager : KTKLocationManager = KTKLocationManager();
    let bluetoothManager : KTKBluetoothManager = KTKBluetoothManager();
    let colors = [
        58795  : UIColor(red: 60/255,    green: 223/255,  blue: 60/255,    alpha: 1),    // green
        //58795  : UIColor(red: 215/255,  green: 40/255,   blue: 40/255,   alpha: 1),      // red
        //52056  : UIColor(red: 60/255,    green: 223/255,  blue: 60/255,    alpha: 1),    // green
        //43564  : UIColor(red: 60/255,    green: 223/255,  blue: 60/255,    alpha: 1),    // green
        //48137  : UIColor(red: 68/255,   green: 129/255,  blue: 172/255,  alpha: 1),      // blue
    ]
    let client : KTKClient = KTKClient();
    
    @IBOutlet var labelCounterBeacons: UILabel!
    
    //@IBOutlet weak var labelCounterBeacons: UILabel!
    //@IBOutlet weak var labelCounterEddystones: UILabel!
    // ------------ LED and Door CURL VARIABLES ------------
    
    var turnLedOn = "https://api.particle.io/v1/devices/55ff6d065075555324151487/prenderLed"
    var bodyData = "arg=led1&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    var bodyData2 = "arg=led2&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    
    var openDoor = "https://api.particle.io/v1/devices/55ff6d065075555324151487/abrirPuerta"
    //var requestOpenDoor = "{ \"arg\": \"A5B7\", \"access_token\": \"54294140ea7a5b2aa3544185e91daf9508eb6d23\" }"
    
    var requestOpenDoor = "arg=A5B7&access_token=54294140ea7a5b2aa3544185e91daf9508eb6d23"
    var LEDFlag = false;
    var DoorFlag = false;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 150, height: 150)
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView!.dataSource = self
        collectionView!.delegate = self
        collectionView!.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView!.backgroundColor = UIColor.whiteColor()
        
        if (KTKLocationManager.canMonitorBeacons())
        {
            let region : KTKRegion = KTKRegion()
            region.uuid = "f7826da6-4fa2-4e98-8024-bc5b71e0893e" // kontakt.io proximity UUID
            
            self.locationManager.setRegions([region])
            self.locationManager.delegate = self
        }
        
        self.bluetoothManager.delegate = self
        //self.client.apiKey = "ktTnlnyDbvOuEwBSBxCjxPgJnKuKExkp"
    }
    
    override func viewDidAppear(animated: Bool) {
        self.locationManager.startMonitoringBeacons()
        self.bluetoothManager.startFindingDevices()
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.locationManager.stopMonitoringBeacons()
        self.bluetoothManager.stopFindingDevices()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - KTKLocationManagerDelegate
    
    func locationManager(locationManager: KTKLocationManager!, didChangeState state: KTKLocationManagerState, withError error: NSError!) {
        if (state == .Failed)
        {
            print("Something went wrong with your Location Services settings. Check your settings.");
        }
    }
    
    func locationManager(locationManager: KTKLocationManager!, didEnterRegion region: KTKRegion!) {
        print("Enter region \(region.identifier)")
    }
    
    func locationManager(locationManager: KTKLocationManager!, didExitRegion region: KTKRegion!) {
        print("Exit region \(region.identifier)")
    }
    
    func locationManager(locationManager: KTKLocationManager!, didRangeBeacons beacons: [AnyObject]!) {
        
        // All the beacons minus all the beacons with proximity 0.
        //let knownBeacons = beacons.filter{ $0.proximity != CLProximity.Unknown }
        let myBlackBeacon = beacons.filter{ $0.minor == 58795}
        // print(knownBeacons)
        let THEBeacon = myBlackBeacon[0] as! CLBeacon;
        print(THEBeacon)
        self.view.backgroundColor = self.colors[Int(THEBeacon.minor)];
        let distance = THEBeacon.proximity.rawValue
        if (distance == 3){
            // FAR : Turn LED ON
            print("The beacon is FAAAAAR away. Doing... Nothing")
            
            //            HTTPPostJSON(turnLedOn, bodyData: bodyData2.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
            //                if error != nil {
            //                    print(error)
            //                } else {
            //                    print(data)
            //
            //                }
            //            })
        } else if(distance == 2){
            // NEAR : Open Door
            print("The beacon is NEARBY. Doing... LED ON")
            if (!LEDFlag){
                
                HTTPPostJSON(turnLedOn, bodyData: bodyData.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
                    if error != nil {
                        print(error)
                    } else {
                        print(data)
                        
                    }
                })
                LEDFlag = true;
            }
            
            
        } else if(distance == 1){
            // RIGHT NEXT : Nothing really
            print("The beacon is RIGHT NEXT. Doing... OPEN DOOR")
            if(!DoorFlag){
                HTTPPostJSON(openDoor, bodyData: requestOpenDoor.dataUsingEncoding(NSUTF8StringEncoding)!, callback: { (data, error:String?) -> Void in
                    if error != nil {
                        print(error)
                    } else {
                        print(data)
                        
                    }
                })
                DoorFlag = true;
            }
            
            
        } else {
            // UNKNOWN : Nothing at all
            print("The beacon is God knows where")
        }
        
        
        // print("There are \(beacons.count) iBeacons monitored")
    }

    // MARK: - KTKBluetoothManagerDelegate
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeDevices devices: Set<NSObject>!) {
        // println("There are \(devices.count) KTKiBeacons devices")
    }
    
    func bluetoothManager(bluetoothManager: KTKBluetoothManager!, didChangeEddystones eddystones: Set<NSObject>!) {
        //println("There are \(eddystones.count) Eddystones in range")
        //self.labelCounterEddystones.text = "\(eddystones.count)"
    }
    
    func HTTPPostJSON(url: String,
        bodyData: NSData, //AnyObject,
        callback: (String, String?) -> Void) {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPMethod = "POST"
            request.addValue("application/x-www-form-urlencoded",
                forHTTPHeaderField: "Content-Type")
            request.HTTPBody = bodyData //data
            HTTPsendRequest(request,callback: callback)
    }
    
    func HTTPsendRequest(request: NSMutableURLRequest, callback: (String, String?) -> Void) {
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(
            request, completionHandler :
            {
                data, response, error in
                if error != nil {
                    callback("", (error!.localizedDescription) as String)
                } else {
                    callback(
                        NSString(data: data!, encoding: NSUTF8StringEncoding) as! String,
                        nil
                    )
                }
        })
        
        task.resume()
    }
    
    
    

    /*override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return 21
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CollectionViewCell
        
       // cell.backgroundColor = UIColor.purpleColor()
        cell.backgroundColor = UIColor(patternImage: UIImage(named: "donaJollyMolly")!)

        if let label = cell.price{
            label.text = "$500"
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    func collectionView(collectionView: UICollectionView!, shouldHighlightItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(collectionView: UICollectionView!, shouldSelectItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    func collectionView(collectionView: UICollectionView!, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return false
    }

    func collectionView(collectionView: UICollectionView!, canPerformAction action: String!, forItemAtIndexPath indexPath: NSIndexPath!, withSender sender: AnyObject!) -> Bool {
        return false
    }

    func collectionView(collectionView: UICollectionView!, performAction action: String!, forItemAtIndexPath indexPath: NSIndexPath!, withSender sender: AnyObject!) {
    
    }
    */
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //NSLog("%d", indexPath.row)
        self.performSegueWithIdentifier("detailSegue", sender: collectionView)
    }
    
    

}
